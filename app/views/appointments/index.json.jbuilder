json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :date, :time, :reason, :fee, :note, :patient_id, :physician_id, :diagnostic_id
  json.url appointment_url(appointment, format: :json)
end
