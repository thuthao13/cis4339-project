json.extract! @appointment, :id, :date, :time, :reason, :fee, :note, :patient_id, :physician_id, :diagnostic_id, :created_at, :updated_at
