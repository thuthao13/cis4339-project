json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :code
  json.url diagnostic_url(diagnostic, format: :json)
end
