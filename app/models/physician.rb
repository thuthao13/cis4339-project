class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, :through => :appointments
  has_many :diagnostics, :through => :appointments

  def full_name
    "Dr. #{first_name} #{last_name}"
  end
end
