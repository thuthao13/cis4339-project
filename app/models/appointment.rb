class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic

  def fee
    errors.add(:fee, "Should be at least 0.01.") if fee.nil? || fee < 0.01
  end

end
