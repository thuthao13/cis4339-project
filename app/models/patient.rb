class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, :through => :appointments
  has_many :diagnostics, :through => :appointments

  def full_name
    "#{first_name} #{last_name}"
  end
end
