class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :date
      t.string :time
      t.text :reason
      t.float :fee
      t.text :note
      t.integer :patient_id
      t.integer :physician_id
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
